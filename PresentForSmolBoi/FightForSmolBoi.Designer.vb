﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FightForSmolBoi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.picEnemyPicture = New System.Windows.Forms.PictureBox()
        Me.btnInitialize = New System.Windows.Forms.Button()
        Me.btnAttackSlot1 = New System.Windows.Forms.Button()
        Me.btnAttackSlot2 = New System.Windows.Forms.Button()
        Me.boxEnemyNameTag = New System.Windows.Forms.TextBox()
        Me.barEnemyHealth = New System.Windows.Forms.ProgressBar()
        Me.barPlayerHealth = New System.Windows.Forms.ProgressBar()
        Me.lblPlayerHealth = New System.Windows.Forms.Label()
        Me.lblEnemyHealth = New System.Windows.Forms.Label()
        Me.lblEnemyAttackStat = New System.Windows.Forms.Label()
        Me.btnAttackSlot3 = New System.Windows.Forms.Button()
        Me.btnAttackSlot4 = New System.Windows.Forms.Button()
        Me.lblPlayerArmorStat = New System.Windows.Forms.Label()
        Me.lblPlayerAttackStat = New System.Windows.Forms.Label()
        Me.lblEnemyArmorStat = New System.Windows.Forms.Label()
        Me.lblPlayerLuckStat = New System.Windows.Forms.Label()
        Me.lblEnemyLuckStat = New System.Windows.Forms.Label()
        Me.lblCombatLog = New System.Windows.Forms.Label()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.lblDebugText = New System.Windows.Forms.Label()
        Me.lblDebugCritState = New System.Windows.Forms.Label()
        Me.lblDebugDodgeState = New System.Windows.Forms.Label()
        Me.lblDebugEnemyAttack1 = New System.Windows.Forms.Label()
        Me.lblDebugEnemyAttack2 = New System.Windows.Forms.Label()
        Me.lblEnemyCombatLog = New System.Windows.Forms.Label()
        Me.lblDebugEnemyCritState = New System.Windows.Forms.Label()
        Me.lblDebugEnemyDodgeState = New System.Windows.Forms.Label()
        Me.mskResultBox = New System.Windows.Forms.MaskedTextBox()
        CType(Me.picEnemyPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picEnemyPicture
        '
        Me.picEnemyPicture.Location = New System.Drawing.Point(98, 33)
        Me.picEnemyPicture.Name = "picEnemyPicture"
        Me.picEnemyPicture.Size = New System.Drawing.Size(182, 190)
        Me.picEnemyPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEnemyPicture.TabIndex = 0
        Me.picEnemyPicture.TabStop = False
        '
        'btnInitialize
        '
        Me.btnInitialize.Location = New System.Drawing.Point(139, 201)
        Me.btnInitialize.Name = "btnInitialize"
        Me.btnInitialize.Size = New System.Drawing.Size(101, 60)
        Me.btnInitialize.TabIndex = 1
        Me.btnInitialize.Text = "Commence"
        Me.btnInitialize.UseVisualStyleBackColor = True
        '
        'btnAttackSlot1
        '
        Me.btnAttackSlot1.Enabled = False
        Me.btnAttackSlot1.Location = New System.Drawing.Point(40, 297)
        Me.btnAttackSlot1.Name = "btnAttackSlot1"
        Me.btnAttackSlot1.Size = New System.Drawing.Size(82, 31)
        Me.btnAttackSlot1.TabIndex = 2
        Me.btnAttackSlot1.Text = "Attack Slot 1"
        Me.btnAttackSlot1.UseVisualStyleBackColor = True
        Me.btnAttackSlot1.Visible = False
        '
        'btnAttackSlot2
        '
        Me.btnAttackSlot2.Enabled = False
        Me.btnAttackSlot2.Location = New System.Drawing.Point(260, 297)
        Me.btnAttackSlot2.Name = "btnAttackSlot2"
        Me.btnAttackSlot2.Size = New System.Drawing.Size(82, 31)
        Me.btnAttackSlot2.TabIndex = 3
        Me.btnAttackSlot2.Text = "Attack Slot 2"
        Me.btnAttackSlot2.UseVisualStyleBackColor = True
        Me.btnAttackSlot2.Visible = False
        '
        'boxEnemyNameTag
        '
        Me.boxEnemyNameTag.Enabled = False
        Me.boxEnemyNameTag.Location = New System.Drawing.Point(118, 7)
        Me.boxEnemyNameTag.Name = "boxEnemyNameTag"
        Me.boxEnemyNameTag.ReadOnly = True
        Me.boxEnemyNameTag.Size = New System.Drawing.Size(142, 20)
        Me.boxEnemyNameTag.TabIndex = 4
        Me.boxEnemyNameTag.Visible = False
        '
        'barEnemyHealth
        '
        Me.barEnemyHealth.BackColor = System.Drawing.Color.Chartreuse
        Me.barEnemyHealth.Location = New System.Drawing.Point(140, 229)
        Me.barEnemyHealth.Name = "barEnemyHealth"
        Me.barEnemyHealth.Size = New System.Drawing.Size(100, 23)
        Me.barEnemyHealth.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.barEnemyHealth.TabIndex = 5
        Me.barEnemyHealth.Visible = False
        '
        'barPlayerHealth
        '
        Me.barPlayerHealth.BackColor = System.Drawing.Color.LimeGreen
        Me.barPlayerHealth.Location = New System.Drawing.Point(139, 327)
        Me.barPlayerHealth.Name = "barPlayerHealth"
        Me.barPlayerHealth.Size = New System.Drawing.Size(100, 23)
        Me.barPlayerHealth.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.barPlayerHealth.TabIndex = 6
        Me.barPlayerHealth.Visible = False
        '
        'lblPlayerHealth
        '
        Me.lblPlayerHealth.AutoSize = True
        Me.lblPlayerHealth.Location = New System.Drawing.Point(153, 353)
        Me.lblPlayerHealth.Name = "lblPlayerHealth"
        Me.lblPlayerHealth.Size = New System.Drawing.Size(70, 13)
        Me.lblPlayerHealth.TabIndex = 7
        Me.lblPlayerHealth.Text = "Player Health"
        Me.lblPlayerHealth.Visible = False
        '
        'lblEnemyHealth
        '
        Me.lblEnemyHealth.AutoSize = True
        Me.lblEnemyHealth.Location = New System.Drawing.Point(153, 255)
        Me.lblEnemyHealth.Name = "lblEnemyHealth"
        Me.lblEnemyHealth.Size = New System.Drawing.Size(73, 13)
        Me.lblEnemyHealth.TabIndex = 8
        Me.lblEnemyHealth.Text = "Enemy Health"
        Me.lblEnemyHealth.Visible = False
        '
        'lblEnemyAttackStat
        '
        Me.lblEnemyAttackStat.AutoSize = True
        Me.lblEnemyAttackStat.Location = New System.Drawing.Point(328, 44)
        Me.lblEnemyAttackStat.Name = "lblEnemyAttackStat"
        Me.lblEnemyAttackStat.Size = New System.Drawing.Size(95, 13)
        Me.lblEnemyAttackStat.TabIndex = 9
        Me.lblEnemyAttackStat.Text = "Enemy Attack Stat"
        Me.lblEnemyAttackStat.Visible = False
        '
        'btnAttackSlot3
        '
        Me.btnAttackSlot3.Enabled = False
        Me.btnAttackSlot3.Location = New System.Drawing.Point(40, 365)
        Me.btnAttackSlot3.Name = "btnAttackSlot3"
        Me.btnAttackSlot3.Size = New System.Drawing.Size(82, 31)
        Me.btnAttackSlot3.TabIndex = 10
        Me.btnAttackSlot3.Text = "Attack Slot 3"
        Me.btnAttackSlot3.UseVisualStyleBackColor = True
        Me.btnAttackSlot3.Visible = False
        '
        'btnAttackSlot4
        '
        Me.btnAttackSlot4.Enabled = False
        Me.btnAttackSlot4.Location = New System.Drawing.Point(260, 365)
        Me.btnAttackSlot4.Name = "btnAttackSlot4"
        Me.btnAttackSlot4.Size = New System.Drawing.Size(82, 31)
        Me.btnAttackSlot4.TabIndex = 11
        Me.btnAttackSlot4.Text = "Attack Slot 4"
        Me.btnAttackSlot4.UseVisualStyleBackColor = True
        Me.btnAttackSlot4.Visible = False
        '
        'lblPlayerArmorStat
        '
        Me.lblPlayerArmorStat.AutoSize = True
        Me.lblPlayerArmorStat.Location = New System.Drawing.Point(368, 315)
        Me.lblPlayerArmorStat.Name = "lblPlayerArmorStat"
        Me.lblPlayerArmorStat.Size = New System.Drawing.Size(88, 13)
        Me.lblPlayerArmorStat.TabIndex = 12
        Me.lblPlayerArmorStat.Text = "Player Armor Stat"
        Me.lblPlayerArmorStat.Visible = False
        '
        'lblPlayerAttackStat
        '
        Me.lblPlayerAttackStat.AutoSize = True
        Me.lblPlayerAttackStat.Location = New System.Drawing.Point(368, 287)
        Me.lblPlayerAttackStat.Name = "lblPlayerAttackStat"
        Me.lblPlayerAttackStat.Size = New System.Drawing.Size(92, 13)
        Me.lblPlayerAttackStat.TabIndex = 13
        Me.lblPlayerAttackStat.Text = "Player Attack Stat"
        Me.lblPlayerAttackStat.Visible = False
        '
        'lblEnemyArmorStat
        '
        Me.lblEnemyArmorStat.AutoSize = True
        Me.lblEnemyArmorStat.Location = New System.Drawing.Point(328, 73)
        Me.lblEnemyArmorStat.Name = "lblEnemyArmorStat"
        Me.lblEnemyArmorStat.Size = New System.Drawing.Size(91, 13)
        Me.lblEnemyArmorStat.TabIndex = 14
        Me.lblEnemyArmorStat.Text = "Enemy Armor Stat"
        Me.lblEnemyArmorStat.Visible = False
        '
        'lblPlayerLuckStat
        '
        Me.lblPlayerLuckStat.AutoSize = True
        Me.lblPlayerLuckStat.Location = New System.Drawing.Point(368, 353)
        Me.lblPlayerLuckStat.Name = "lblPlayerLuckStat"
        Me.lblPlayerLuckStat.Size = New System.Drawing.Size(85, 13)
        Me.lblPlayerLuckStat.TabIndex = 15
        Me.lblPlayerLuckStat.Text = "Player Luck Stat"
        Me.lblPlayerLuckStat.Visible = False
        '
        'lblEnemyLuckStat
        '
        Me.lblEnemyLuckStat.AutoSize = True
        Me.lblEnemyLuckStat.Location = New System.Drawing.Point(328, 98)
        Me.lblEnemyLuckStat.Name = "lblEnemyLuckStat"
        Me.lblEnemyLuckStat.Size = New System.Drawing.Size(88, 13)
        Me.lblEnemyLuckStat.TabIndex = 16
        Me.lblEnemyLuckStat.Text = "Enemy Luck Stat"
        Me.lblEnemyLuckStat.Visible = False
        '
        'lblCombatLog
        '
        Me.lblCombatLog.AutoSize = True
        Me.lblCombatLog.Location = New System.Drawing.Point(343, 182)
        Me.lblCombatLog.Name = "lblCombatLog"
        Me.lblCombatLog.Size = New System.Drawing.Size(64, 13)
        Me.lblCombatLog.TabIndex = 17
        Me.lblCombatLog.Text = "Combat Log"
        Me.lblCombatLog.Visible = False
        '
        'btnStop
        '
        Me.btnStop.Location = New System.Drawing.Point(12, 12)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(75, 23)
        Me.btnStop.TabIndex = 18
        Me.btnStop.Text = "Stop"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'lblDebugText
        '
        Me.lblDebugText.AutoSize = True
        Me.lblDebugText.Location = New System.Drawing.Point(528, 327)
        Me.lblDebugText.Name = "lblDebugText"
        Me.lblDebugText.Size = New System.Drawing.Size(81, 13)
        Me.lblDebugText.TabIndex = 19
        Me.lblDebugText.Text = "Debug Execute"
        '
        'lblDebugCritState
        '
        Me.lblDebugCritState.AutoSize = True
        Me.lblDebugCritState.Location = New System.Drawing.Point(528, 297)
        Me.lblDebugCritState.Name = "lblDebugCritState"
        Me.lblDebugCritState.Size = New System.Drawing.Size(85, 13)
        Me.lblDebugCritState.TabIndex = 20
        Me.lblDebugCritState.Text = "Debug Crit State"
        '
        'lblDebugDodgeState
        '
        Me.lblDebugDodgeState.AutoSize = True
        Me.lblDebugDodgeState.Location = New System.Drawing.Point(528, 353)
        Me.lblDebugDodgeState.Name = "lblDebugDodgeState"
        Me.lblDebugDodgeState.Size = New System.Drawing.Size(102, 13)
        Me.lblDebugDodgeState.TabIndex = 21
        Me.lblDebugDodgeState.Text = "Debug Dodge State"
        '
        'lblDebugEnemyAttack1
        '
        Me.lblDebugEnemyAttack1.AutoSize = True
        Me.lblDebugEnemyAttack1.Location = New System.Drawing.Point(479, 44)
        Me.lblDebugEnemyAttack1.Name = "lblDebugEnemyAttack1"
        Me.lblDebugEnemyAttack1.Size = New System.Drawing.Size(117, 13)
        Me.lblDebugEnemyAttack1.TabIndex = 22
        Me.lblDebugEnemyAttack1.Text = "Debug Enemy Attack 1"
        '
        'lblDebugEnemyAttack2
        '
        Me.lblDebugEnemyAttack2.AutoSize = True
        Me.lblDebugEnemyAttack2.Location = New System.Drawing.Point(479, 73)
        Me.lblDebugEnemyAttack2.Name = "lblDebugEnemyAttack2"
        Me.lblDebugEnemyAttack2.Size = New System.Drawing.Size(117, 13)
        Me.lblDebugEnemyAttack2.TabIndex = 23
        Me.lblDebugEnemyAttack2.Text = "Debug Enemy Attack 2"
        '
        'lblEnemyCombatLog
        '
        Me.lblEnemyCombatLog.AutoSize = True
        Me.lblEnemyCombatLog.Location = New System.Drawing.Point(343, 225)
        Me.lblEnemyCombatLog.Name = "lblEnemyCombatLog"
        Me.lblEnemyCombatLog.Size = New System.Drawing.Size(99, 13)
        Me.lblEnemyCombatLog.TabIndex = 24
        Me.lblEnemyCombatLog.Text = "Enemy Combat Log"
        Me.lblEnemyCombatLog.Visible = False
        '
        'lblDebugEnemyCritState
        '
        Me.lblDebugEnemyCritState.AutoSize = True
        Me.lblDebugEnemyCritState.Location = New System.Drawing.Point(479, 98)
        Me.lblDebugEnemyCritState.Name = "lblDebugEnemyCritState"
        Me.lblDebugEnemyCritState.Size = New System.Drawing.Size(120, 13)
        Me.lblDebugEnemyCritState.TabIndex = 26
        Me.lblDebugEnemyCritState.Text = "Debug Enemy Crit State"
        '
        'lblDebugEnemyDodgeState
        '
        Me.lblDebugEnemyDodgeState.AutoSize = True
        Me.lblDebugEnemyDodgeState.Location = New System.Drawing.Point(479, 126)
        Me.lblDebugEnemyDodgeState.Name = "lblDebugEnemyDodgeState"
        Me.lblDebugEnemyDodgeState.Size = New System.Drawing.Size(137, 13)
        Me.lblDebugEnemyDodgeState.TabIndex = 27
        Me.lblDebugEnemyDodgeState.Text = "Debug Enemy Dodge State"
        '
        'mskResultBox
        '
        Me.mskResultBox.AllowPromptAsInput = False
        Me.mskResultBox.BackColor = System.Drawing.Color.Lime
        Me.mskResultBox.Font = New System.Drawing.Font("Tempus Sans ITC", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mskResultBox.ForeColor = System.Drawing.SystemColors.InfoText
        Me.mskResultBox.Location = New System.Drawing.Point(98, 104)
        Me.mskResultBox.Name = "mskResultBox"
        Me.mskResultBox.ReadOnly = True
        Me.mskResultBox.Size = New System.Drawing.Size(182, 24)
        Me.mskResultBox.TabIndex = 28
        Me.mskResultBox.Visible = False
        '
        'FightForSmolBoi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 408)
        Me.Controls.Add(Me.mskResultBox)
        Me.Controls.Add(Me.lblDebugEnemyDodgeState)
        Me.Controls.Add(Me.lblDebugEnemyCritState)
        Me.Controls.Add(Me.lblEnemyCombatLog)
        Me.Controls.Add(Me.lblDebugEnemyAttack2)
        Me.Controls.Add(Me.lblDebugEnemyAttack1)
        Me.Controls.Add(Me.lblDebugDodgeState)
        Me.Controls.Add(Me.lblDebugCritState)
        Me.Controls.Add(Me.lblDebugText)
        Me.Controls.Add(Me.btnStop)
        Me.Controls.Add(Me.lblCombatLog)
        Me.Controls.Add(Me.lblEnemyLuckStat)
        Me.Controls.Add(Me.lblPlayerLuckStat)
        Me.Controls.Add(Me.lblEnemyArmorStat)
        Me.Controls.Add(Me.lblPlayerAttackStat)
        Me.Controls.Add(Me.lblPlayerArmorStat)
        Me.Controls.Add(Me.btnAttackSlot4)
        Me.Controls.Add(Me.btnAttackSlot3)
        Me.Controls.Add(Me.lblEnemyAttackStat)
        Me.Controls.Add(Me.lblEnemyHealth)
        Me.Controls.Add(Me.lblPlayerHealth)
        Me.Controls.Add(Me.barPlayerHealth)
        Me.Controls.Add(Me.barEnemyHealth)
        Me.Controls.Add(Me.boxEnemyNameTag)
        Me.Controls.Add(Me.btnAttackSlot2)
        Me.Controls.Add(Me.btnAttackSlot1)
        Me.Controls.Add(Me.btnInitialize)
        Me.Controls.Add(Me.picEnemyPicture)
        Me.Name = "FightForSmolBoi"
        Me.Text = "Form1"
        CType(Me.picEnemyPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picEnemyPicture As PictureBox
    Friend WithEvents btnInitialize As Button
    Friend WithEvents btnAttackSlot1 As Button
    Friend WithEvents btnAttackSlot2 As Button
    Friend WithEvents boxEnemyNameTag As TextBox
    Friend WithEvents barEnemyHealth As ProgressBar
    Friend WithEvents barPlayerHealth As ProgressBar
    Friend WithEvents lblPlayerHealth As Label
    Friend WithEvents lblEnemyHealth As Label
    Friend WithEvents lblEnemyAttackStat As Label
    Friend WithEvents btnAttackSlot3 As Button
    Friend WithEvents btnAttackSlot4 As Button
    Friend WithEvents lblPlayerArmorStat As Label
    Friend WithEvents lblPlayerAttackStat As Label
    Friend WithEvents lblEnemyArmorStat As Label
    Friend WithEvents lblPlayerLuckStat As Label
    Friend WithEvents lblEnemyLuckStat As Label
    Friend WithEvents lblCombatLog As Label
    Friend WithEvents btnStop As Button
    Friend WithEvents lblDebugText As Label
    Friend WithEvents lblDebugCritState As Label
    Friend WithEvents lblDebugDodgeState As Label
    Friend WithEvents lblDebugEnemyAttack1 As Label
    Friend WithEvents lblDebugEnemyAttack2 As Label
    Friend WithEvents lblEnemyCombatLog As Label
    Friend WithEvents lblDebugEnemyCritState As Label
    Friend WithEvents lblDebugEnemyDodgeState As Label
    Friend WithEvents mskResultBox As MaskedTextBox
End Class
