﻿Public Class FightForSmolBoi
    Public initializedTarget = New Integer(1) {0, 1}
    Public initializedPlayerAttacks = New String(3) {0, 1, 2, 3}
    Public initializedEnemyAttacks = New String(1) {0, 1}
    Public playerAttackName = New String(7) {"Slapp", "Wakk", "Schmak", "Clubb", "Blastt", "'Splode", "Slamm", "Slaash"}
    Public enemyAttackName = New String(3) {"Crushes", "Body Slams", "Gnashes", "Rends"}
    Public currentAttack As String = ""
    Public currentDodge As Boolean = False
    Public currentCrit As Boolean = False
    Public relativeAttackDamage As New Single
    Public relativeAttackArmorFactor As New Single
    Public currentAttackDamage As New Single
    Public playerMaxHealth As New Integer
    Public enemyMaxHealth As New Integer
    Public playerCurrentHealth As New Integer
    Public enemyCurrentHealth As New Integer
    Public playerArmor As New Integer
    Public enemyArmor As New Integer
    Public playerLuck As New Integer
    Public enemyLuck As New Integer
    Public playerNameTag As String = ""
    Public enemyNameTag As String = ""
    Public playerAttackPower As New Integer
    Public enemyAttackPower As New Integer
    Public ReadOnly RollAttackSpeed As Short = 0.5
    Public WithEvents rndTimer As New Timer
    Public rndValue As Decimal
    Public statRndValue As Decimal
    Public attackRndValue As Decimal

    Private Sub commenceRnd() Handles Me.Load
        Randomize()
        rndTimer.Interval = 18
        rndTimer.Start()
    End Sub
    Private Sub attackRndValueTimer() Handles rndTimer.Tick
        attackRndValue = Rnd()
        statRndValue = Rnd() + (Rnd() / 5)
        rndValue = Rnd()
    End Sub
    'Initialization procedures
    Private Sub btnInitializeBattle(sender As Object, e As EventArgs) Handles btnInitialize.Click
        boxEnemyNameTag.Enabled = True
        boxEnemyNameTag.Visible = True
        btnInitialize.Visible = False
        btnInitialize.Enabled = False
        barEnemyHealth.Visible = True
        barPlayerHealth.Visible = True
        lblEnemyHealth.Visible = True
        lblPlayerHealth.Visible = True
        lblEnemyAttackStat.Visible = True
        lblEnemyArmorStat.Visible = True
        lblPlayerAttackStat.Visible = True
        lblPlayerArmorStat.Visible = True
        lblPlayerLuckStat.Visible = True
        lblEnemyLuckStat.Visible = True
        btnAttackSlot1.Visible = True
        btnAttackSlot2.Visible = True
        btnAttackSlot3.Visible = True
        btnAttackSlot4.Visible = True
        btnAttackSlot1.Enabled = True
        btnAttackSlot2.Enabled = True
        btnAttackSlot3.Enabled = True
        btnAttackSlot4.Enabled = True
        boxEnemyNameTag.Text = findEnemyName()
        enemyMaxHealth = findEnemyHealth()
        enemyCurrentHealth = enemyMaxHealth
        lblEnemyHealth.Text = enemyMaxHealth
        picEnemyPicture.Image = findImage()
        barEnemyHealth.Maximum = 100
        barEnemyHealth.Minimum = 0
        barEnemyHealth.Value = findEnemyHealthPercent()
        lblEnemyHealth.Text = Convert.ToString(enemyMaxHealth)
        playerMaxHealth = findPlayerMaxHealth()
        lblPlayerHealth.Text = Convert.ToString(playerMaxHealth)
        playerCurrentHealth = playerMaxHealth
        barPlayerHealth.Maximum = 100
        barPlayerHealth.Minimum = 0
        barPlayerHealth.Value = findPlayerHealthPercent()
        enemyAttackPower = findEnemyAttackPower()
        lblEnemyAttackStat.Text = "Attack: " & Convert.ToString(enemyAttackPower)
        playerAttackPower = findPlayerAttackPower()
        lblPlayerAttackStat.Text = "Attack: " & Convert.ToString(playerAttackPower)
        enemyArmor = findEnemyArmor()
        lblEnemyArmorStat.Text = "Armor: " & Convert.ToString(enemyArmor)
        playerArmor = findPlayerArmor()
        lblPlayerArmorStat.Text = "Armor: " & Convert.ToString(playerArmor)
        enemyLuck = genericFindLuck()
        lblEnemyLuckStat.Text = "Luck: " & Convert.ToString(enemyLuck)
        playerLuck = genericFindLuck()
        lblPlayerLuckStat.Text = "Luck:" & Convert.ToString(playerLuck)
        initializedPlayerAttacks(0) = findFirstAttack()
        btnAttackSlot1.Text = initializedPlayerAttacks(0)
        initializedPlayerAttacks(1) = findSecondAttack()
        btnAttackSlot2.Text = initializedPlayerAttacks(1)
        initializedPlayerAttacks(2) = findThirdAttack()
        btnAttackSlot3.Text = initializedPlayerAttacks(2)
        initializedPlayerAttacks(3) = findFourthAttack()
        btnAttackSlot4.Text = initializedPlayerAttacks(3)
        initializedEnemyAttacks(0) = findEnemyAttack1()
        lblDebugEnemyAttack1.Text = initializedEnemyAttacks(0)
        initializedEnemyAttacks(1) = findEnemyAttack2()
        lblDebugEnemyAttack1.Text = initializedEnemyAttacks(0)
        lblDebugEnemyAttack2.Text = initializedEnemyAttacks(1)

    End Sub
    Private Function findEnemyName()
        If rndValue >= 0.5 Then
            enemyNameTag = "Hogger"
            Return "Hogger"
        ElseIf rndValue < 0.5 Then
            enemyNameTag = "Great Blobfish"
            Return "Great Blobfish"
        Else
            Return "Error"
        End If

    End Function
    Private Function findEnemyHealth()
        If enemyNameTag = "Hogger" Then
            Return (statRndValue * 50) + 200
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return (statRndValue * 80) + 260
        Else Return 1
        End If

    End Function
    Private Function findImage()
        If enemyNameTag = "Hogger" Then
            Return Image.FromFile("C:\Visual Studio Pictures\Hogger.jpg")
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return Image.FromFile("C:\Visual Studio Pictures\Blobfish.jpg")
        Else Return Nothing
        End If
    End Function
    Private Function findEnemyHealthPercent()
        Return Int(100 * (enemyCurrentHealth / enemyMaxHealth))
    End Function
    Private Function findPlayerMaxHealth()
        If enemyNameTag = "Hogger" Then
            Return Int(((0.15 * Rnd() + 0.9) * 70) + 150)
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return Int(((0.15 * Rnd() + 0.9) * 85) + 175)
        Else
            Return 0
        End If
    End Function
    Private Function findPlayerHealthPercent()
        Return Int(100 * (playerCurrentHealth / playerMaxHealth))
    End Function
    Private Function findEnemyAttackPower()
        If enemyNameTag = "Hogger" Then
            Return Int((Rnd() * 2) + 21)
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return Int((Rnd() * 2) + 24)
        Else
            Return 0
        End If

    End Function
    Private Function findPlayerAttackPower()
        If enemyNameTag = "Hogger" Then
            Return Int((Rnd() * 3) + 17)
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return Int((Rnd() * 5) + 21)
        Else Return 0

        End If

    End Function
    Private Function findEnemyArmor()
        If enemyNameTag = "Hogger" Then
            Return Int((Rnd() * 4) + 3)
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return Int((Rnd() * 4) + 5)
        Else Return 0


        End If

    End Function
    Private Function findPlayerArmor()
        If enemyNameTag = "Hogger" Then
            Return Int((Rnd() * 2) + 3)
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return Int((Rnd() * 3) + 3.75)
        Else Return 0

        End If

    End Function
    Private Function genericFindLuck()
        If enemyNameTag = "Hogger" Then
            Return Int((Rnd() * 70))
        ElseIf enemyNameTag = "Great Blobfish" Then
            Return Int((Rnd() * 70))
        Else Return 0

        End If

    End Function
    Private Function findFirstAttack()
        If attackRndValue >= 0.5 Then
            Return playerAttackName(0)
        ElseIf attackRndValue < 0.5 Then
            Return playerAttackName(1)
        Else Return Nothing

        End If

    End Function
    Private Function findSecondAttack()
        If attackRndValue >= 0.5 Then
            Return playerAttackName(2)
        ElseIf attackRndValue < 0.5 Then
            Return playerAttackName(3)
        Else Return Nothing

        End If

    End Function
    Private Function findThirdAttack()
        If rndValue >= 0.45 Then
            Return playerAttackName(4)
        ElseIf rndValue < 0.45 Then
            Return playerAttackName(5)
        Else Return Nothing

        End If

    End Function
    Private Function findFourthAttack()
        If rndValue >= 0.45 Then
            Return playerAttackName(6)
        ElseIf rndValue < 0.45 Then
            Return playerAttackName(7)
        Else Return Nothing

        End If

    End Function
    Private Function findEnemyAttack1()
        If rndValue > 0.5 Then
            Return enemyAttackName(0)
        Else
            Return enemyAttackName(1)
        End If
    End Function
    Private Function findEnemyAttack2()
        If rndValue > 0.45 Then
            Return enemyAttackName(2)
        Else
            Return enemyAttackName(3)
        End If
    End Function
    'Repeatable Procedures
    Private Sub playerAttack(sender As Button, e As EventArgs) Handles btnAttackSlot1.Click, btnAttackSlot2.Click, btnAttackSlot3.Click, btnAttackSlot4.Click
        currentDodge = False
        currentCrit = False
        If sender Is btnAttackSlot1 Then
            attackSlot1Handle()
        ElseIf sender Is btnAttackSlot2 Then
            attackSlot2Handle()
        ElseIf sender Is btnAttackSlot3 Then
            attackSlot3Handle()
        ElseIf sender Is btnAttackSlot4 Then
            attackslot4Handle()
        Else
            lblCombatLog.Text = "Button Handler Error"
        End If

    End Sub
    Private Sub attackSlot1Handle()
        If btnAttackSlot1.Text = playerAttackName(0) Then
            relativeAttackDamage = 1
            relativeAttackArmorFactor = 1.125
            currentAttack = playerAttackName(0)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        ElseIf btnAttackSlot1.Text = playerAttackName(1) Then
            relativeAttackDamage = 1.15
            relativeAttackArmorFactor = 1.25
            currentAttack = playerAttackName(1)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        Else

        End If

    End Sub
    Private Sub attackSlot2Handle()
        If btnAttackSlot2.Text = playerAttackName(2) Then
            relativeAttackDamage = 0.8
            relativeAttackArmorFactor = 0.75
            currentAttack = playerAttackName(2)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        ElseIf btnAttackSlot2.Text = playerAttackName(3) Then
            relativeAttackDamage = 0.725
            relativeAttackArmorFactor = 0.685
            currentAttack = playerAttackName(3)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        Else

        End If

    End Sub
    Private Sub attackSlot3Handle()
        If btnAttackSlot3.Text = playerAttackName(4) Then
            relativeAttackDamage = 1.3
            relativeAttackArmorFactor = 2
            currentAttack = playerAttackName(4)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        ElseIf btnAttackSlot3.Text = playerAttackName(5) Then
            relativeAttackDamage = 1.35
            relativeAttackArmorFactor = 2.15
            currentAttack = playerAttackName(5)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        Else

        End If

    End Sub
    Private Sub attackslot4Handle()
        If btnAttackSlot4.Text = playerAttackName(6) Then
            relativeAttackDamage = 0.5
            relativeAttackArmorFactor = 0.2
            currentAttack = playerAttackName(6)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        ElseIf btnAttackSlot4.Text = playerAttackName(7) Then
            relativeAttackDamage = 3
            relativeAttackArmorFactor = 7.25
            currentAttack = playerAttackName(7)
            rollDodgeEnemy()
            rollCritFriendlyDecide()
            rollGenericFriendlyDamage()
        End If
    End Sub
    Private Sub rollDodgeEnemy()
        If Int(((0.5 * rndValue) + 0.5) * enemyLuck / 2) > 20 Then
            currentDodge = True
        Else
            currentDodge = False
        End If

        Return
    End Sub
    Private Sub rollCritFriendlyDecide()
        If currentDodge = True Then

        Else rollCritFriendly()

        End If

        Return
    End Sub
    Private Sub rollCritFriendly()
        If Int(((0.5 * rndValue) + 0.5) * playerLuck / 2) > 20 Then
            currentCrit = True
        Else
            currentCrit = False
        End If

        Return
    End Sub
    Private Sub rollGenericFriendlyDamage()
        currentAttackDamage = Int((((1.1 * rndValue) + 0.9) * playerAttackPower * relativeAttackDamage) - ((1.15 * rndValue) + 0.9) * enemyArmor * relativeAttackArmorFactor)
        If currentCrit = True And currentDodge = False And currentAttackDamage >= 0 Then
            enemyCurrentHealth = enemyCurrentHealth - (1.35 * (currentAttackDamage))
        ElseIf currentCrit = False And currentDodge = False And currentAttackDamage >= 0 Then
            enemyCurrentHealth = enemyCurrentHealth - currentAttackDamage
        ElseIf currentDodge = False And currentAttackDamage < 0 Then
            'damage is negative and won't be dealt
            currentAttackDamage = 0
        End If
        lblCombatLog.Visible = True
        lblDebugText.Text = "Rolled Friendly Damage"
        lblDebugCritState.Text = "Crit: " & Convert.ToString(currentCrit)
        lblDebugDodgeState.Text = "Dodge: " & Convert.ToString(currentDodge)
        lblEnemyHealth.Text = enemyCurrentHealth
        checkValidEnemyLife()
        barEnemyHealth.Value = findEnemyHealthPercent()
        barEnemyHealth.Update()
        lblEnemyHealth.Update()
        printLogFriendly()
        checkenemyLife()


    End Sub
    Private Sub checkValidEnemyLife()
        If enemyCurrentHealth < 0 Then
            enemyCurrentHealth = 0
        Else

        End If
    End Sub
    Private Sub rollGenericEnemyDamageSequence()
        rollCritEnemy()
        rollDodgeFriendly()
        rollEnemyAttackDecider()
        rollGenericEnemyDamage()
    End Sub
    Private Sub printLogFriendly()
        If currentCrit = True And currentDodge = False And currentAttackDamage > 0 Then
            lblCombatLog.Text = "You " & currentAttack & " and crit " & enemyNameTag & " for " & Convert.ToString(currentAttackDamage) & " damage!"
        ElseIf currentCrit = False And currentDodge = False And currentAttackDamage > 0 Then
            lblCombatLog.Text = "You " & currentAttack & " " & enemyNameTag & " for " & Convert.ToString(currentAttackDamage) & " damage!"
        ElseIf currentDodge = True Then
            lblCombatLog.Text = "Your " & currentAttack & " missed!"
        ElseIf currentAttackDamage = 0 Then
            lblCombatLog.Text = "You deal no damage!"
        Else

        End If
    End Sub
    Private Sub rollEnemyAttackDecider()
        If rndValue > 0.5 Then
            currentAttack = initializedEnemyAttacks(0)
            enemyAttackSlot1Handle()
        Else
            currentAttack = initializedEnemyAttacks(1)
            enemyAttackSlot2Handle()
        End If

    End Sub
    Private Sub enemyAttackSlot1Handle()
        If initializedEnemyAttacks(0) = enemyAttackName(0) Then
            relativeAttackDamage = 1.15
            relativeAttackArmorFactor = 1.05
        ElseIf initializedEnemyAttacks(0) = enemyAttackName(1) Then
            relativeAttackDamage = 1.275
            relativeAttackArmorFactor = 1.25
        Else

        End If
    End Sub
    Private Sub enemyAttackSlot2Handle()
        If initializedEnemyAttacks(1) = enemyAttackName(2) Then
            relativeAttackDamage = 0.75
            relativeAttackArmorFactor = 0.8
        ElseIf initializedEnemyAttacks(1) = enemyAttackName(3) Then
            relativeAttackDamage = 0.4
            relativeAttackArmorFactor = 0.05
        End If

    End Sub
    Private Sub rollDodgeFriendly()
        If Int(((0.5 * Rnd()) + 0.5) * playerLuck) / 2 > 25 Then
            currentDodge = True
        Else
            currentDodge = False
        End If

        Return
    End Sub
    Private Sub rollCritEnemy()
        If Int(((0.5 * Rnd()) + 0.5) * enemyLuck / 2) > 25 Then
            currentCrit = True
        Else
            currentCrit = False
        End If

        Return
    End Sub
    Private Sub rollGenericEnemyDamage()
        currentAttackDamage = Int((((1.1 * rndValue) + 0.9) * playerAttackPower * relativeAttackDamage) - ((1.15 * rndValue) + 0.9) * enemyArmor * relativeAttackArmorFactor)
        If currentCrit = True And currentDodge = False And currentAttackDamage >= 0 Then
            playerCurrentHealth = playerCurrentHealth - (1.35 * (currentAttackDamage))
        ElseIf currentCrit = False And currentDodge = False And currentAttackDamage >= 0 Then
            playerCurrentHealth = playerCurrentHealth - currentAttackDamage
        ElseIf currentDodge = False And currentAttackDamage < 0 Then
            'damage is negative and won't be dealt
            currentAttackDamage = 0
        End If
        checkValidLifePlayer()
        barPlayerHealth.Value = findPlayerHealthPercent()
        printLogEnemy()
    End Sub
    Private Sub checkValidLifePlayer()
        If playerCurrentHealth < 0 Then
            playerCurrentHealth = 0
        Else

        End If
    End Sub
    Private Sub printLogEnemy()
        If currentCrit = True And currentDodge = False And currentAttackDamage > 0 Then
            lblEnemyCombatLog.Text = enemyNameTag & " " & currentAttack & " and crits you for " & Convert.ToString(currentAttackDamage)
        ElseIf currentCrit = False And currentDodge = False And currentAttackDamage > 0 Then
            lblEnemyCombatLog.Text = enemyNameTag & " " & currentAttack & " you for " & Convert.ToString(currentAttackDamage)
        ElseIf currentDodge = True Then
            lblEnemyCombatLog.Text = enemyNameTag & " " & currentAttack & " and misses!"
        ElseIf currentAttackDamage = 0 Then
            lblEnemyCombatLog.Text = enemyNameTag & " dealt no damage"
        Else

        End If
        lblEnemyCombatLog.Visible = True
        lblPlayerHealth.Text = playerCurrentHealth
        lblPlayerHealth.Update()
        lblDebugEnemyCritState.Text = "Crit: " & Convert.ToString(currentCrit)
        lblDebugEnemyDodgeState.Text = "Dodge: " & Convert.ToString(currentDodge)
    End Sub
    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        Close()
    End Sub
    Private Sub checkEnemyLife()
        If enemyCurrentHealth < 1 Then
            victory()
        Else
            checkPlayerLife()
        End If
    End Sub
    Private Sub checkPlayerLife()
        If playerCurrentHealth < 1 Then
            defeat()
        Else
            rollGenericEnemyDamageSequence()
        End If
    End Sub
    Private Sub victory()
        btnAttackSlot1.Enabled = False
        btnAttackSlot2.Enabled = False
        btnAttackSlot3.Enabled = False
        btnAttackSlot4.Enabled = False
        btnStop.Enabled = False
        mskResultBox.Visible = True
        mskResultBox.Text = enemyNameTag & " is Defeated!"
    End Sub
    Private Sub defeat()
        btnAttackSlot1.Enabled = False
        btnAttackSlot2.Enabled = False
        btnAttackSlot3.Enabled = False
        btnAttackSlot4.Enabled = False
        btnStop.Enabled = False
        mskResultBox.Visible = True
        mskResultBox.BackColor = Color.Red
        mskResultBox.Text = enemyNameTag & " is victorious!"

    End Sub
End Class
